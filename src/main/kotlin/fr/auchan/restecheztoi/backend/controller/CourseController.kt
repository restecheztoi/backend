package fr.auchan.restecheztoi.backend.controller

import fr.auchan.restecheztoi.backend.entity.Course
import fr.auchan.restecheztoi.backend.enumeration.State
import fr.auchan.restecheztoi.backend.service.CourseService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*

@Controller
@RequestMapping("/courses")
class CourseController(@Autowired private val courseService: CourseService) {

    @GetMapping
    fun findAll(@RequestParam studentId: String?, @RequestParam parentId: String?, @RequestParam state: State) = try {
        ResponseEntity.ok(courseService.findAll(studentId, parentId, state))
    } catch (e: IllegalStateException) {
        ResponseEntity.badRequest()
    }

    @PostMapping
    fun create(@RequestBody course: Course) = ResponseEntity.ok(courseService.create(course));

    @PutMapping("/{id}")
    fun update(@PathVariable id: String, @RequestBody course: Course) = try {
        ResponseEntity.ok(courseService.update(id, course))
    } catch (e: NoSuchElementException) {
        ResponseEntity.noContent()
    }

}