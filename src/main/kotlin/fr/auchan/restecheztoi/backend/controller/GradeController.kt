package fr.auchan.restecheztoi.backend.controller

import fr.auchan.restecheztoi.backend.entity.Grade
import fr.auchan.restecheztoi.backend.service.GradeService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*

@Controller
@RequestMapping("/grades")
class GradeController(@Autowired private val gradeService: GradeService) {

    @PostMapping
    fun create(@RequestBody grade: Grade) = ResponseEntity.ok(gradeService.create(grade));

    @GetMapping("/{id}")
    fun findById(@PathVariable id: String) = ResponseEntity.ok(gradeService.findById(id))

    @GetMapping()
    fun findByStudents(@RequestParam studentId: String?) = ResponseEntity.ok(gradeService.findByStudents(studentId))

    @PutMapping("/{id}")
    fun update(@PathVariable id: String, @RequestBody grade: Grade) = try {
        ResponseEntity.ok(gradeService.update(id, grade))
    } catch(e: NoSuchElementException) {
        ResponseEntity.noContent()
    }

    @DeleteMapping("/{id}")
    fun delete(@PathVariable id: String) = try {
        ResponseEntity.ok(gradeService.delete(id))
    } catch(e: NoSuchElementException) {
        ResponseEntity.noContent()
    }

}