package fr.auchan.restecheztoi.backend.controller

import fr.auchan.restecheztoi.backend.entity.Subject
import fr.auchan.restecheztoi.backend.service.SubjectService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*

@Controller
@RequestMapping("/subjects")
class SubjectController(@Autowired private val subjectService: SubjectService) {

    @PostMapping
    fun create(@RequestBody subject: Subject) = ResponseEntity.ok(subjectService.create(subject));

    @GetMapping("/{id}")
    fun findById(@PathVariable id: String) = ResponseEntity.ok(subjectService.findById(id))

    @GetMapping()
    fun findByStudents(@RequestParam studentId: String?) = ResponseEntity.ok(subjectService.findByStudents(studentId))

    @PutMapping("/{id}")
    fun update(@PathVariable id: String, @RequestBody subject: Subject) = try {
        ResponseEntity.ok(subjectService.update(id, subject))
    } catch(e: NoSuchElementException) {
        ResponseEntity.noContent()
    }

    @DeleteMapping("/{id}")
    fun delete(@PathVariable id: String) = try {
        ResponseEntity.ok(subjectService.delete(id))
    } catch(e: NoSuchElementException) {
        ResponseEntity.noContent()
    }

}