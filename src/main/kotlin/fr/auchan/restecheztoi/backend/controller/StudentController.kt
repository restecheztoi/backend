package fr.auchan.restecheztoi.backend.controller

import fr.auchan.restecheztoi.backend.entity.Student
import fr.auchan.restecheztoi.backend.service.StudentService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*

@Controller
@RequestMapping("/students")
class StudentController(@Autowired private val studentService: StudentService) {

    @GetMapping("/{id}")
    fun findById(@PathVariable id: String) = ResponseEntity.ok(studentService.findById(id))

    @GetMapping()
    fun findByGradesAndSubjects(@RequestParam gradeId: String, @RequestParam subjectId: String) = try {
        ResponseEntity.ok(studentService.findByGradesAndSubjects(gradeId, subjectId))
    } catch (e: NoSuchElementException) {
        ResponseEntity.badRequest()
    }

    @PutMapping("/{id}")
    fun update(@PathVariable id: String, @RequestBody student: Student) = try {
        ResponseEntity.ok(studentService.createOrUpdate(id, student))
    } catch(e: NoSuchElementException) {
        ResponseEntity.noContent()
    }

    @DeleteMapping("/{id}")
    fun delete(@PathVariable id: String) = try {
        ResponseEntity.ok(studentService.delete(id))
    } catch(e: NoSuchElementException) {
        ResponseEntity.noContent()
    }

}