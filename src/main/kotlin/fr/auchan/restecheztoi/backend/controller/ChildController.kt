package fr.auchan.restecheztoi.backend.controller

import fr.auchan.restecheztoi.backend.entity.Child
import fr.auchan.restecheztoi.backend.service.ChildService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*

@Controller
@RequestMapping("/childs")
class ChildController(@Autowired private val childService: ChildService) {

    @PostMapping
    fun create(@RequestBody child: Child) = ResponseEntity.ok(childService.create(child));

    @GetMapping
    fun findByParentId(@RequestParam parentId: String) = ResponseEntity.ok(childService.findByParentId(parentId))

    @DeleteMapping("/{id}")
    fun delete(@PathVariable id: String) = try {
        ResponseEntity.ok(childService.delete(id))
    } catch(e: NoSuchElementException) {
        ResponseEntity.noContent()
    }

}