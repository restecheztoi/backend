package fr.auchan.restecheztoi.backend.enumeration

enum class State {
    AWAITING,
    VALIDATED,
    REFUSED,
    DONE
}