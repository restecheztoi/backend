package fr.auchan.restecheztoi.backend.entity

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import java.util.*
import javax.persistence.*

@Entity(name = "child")
@JsonIgnoreProperties("courses")
class Child(
        @Id
        var id: String = UUID.randomUUID().toString(),
        val firstname: String,
        val age: Int,
        val parentId: String,
        @OneToMany(mappedBy="child")
        val courses: Collection<Course> = listOf(),
        @ManyToOne(fetch = FetchType.LAZY)
        val grade: Grade
)