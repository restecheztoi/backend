package fr.auchan.restecheztoi.backend.entity

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import java.util.*
import javax.persistence.*

@Entity(name = "subject")
@JsonIgnoreProperties("students", "courses")
class Subject(
        @Id
        var id: String = UUID.randomUUID().toString(),
        @ManyToMany(mappedBy = "subjects")
        var students: List<Student> = listOf(),
        @OneToMany(mappedBy="subject")
        val courses: Collection<Course> = listOf(),
        val label: String
)