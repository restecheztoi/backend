package fr.auchan.restecheztoi.backend.entity

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import java.util.*
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.ManyToMany
import javax.persistence.OneToMany

@Entity(name = "grade")
@JsonIgnoreProperties("students", "childs")
class Grade(
        @Id
        var id: String = UUID.randomUUID().toString(),
        val label: String,
        @ManyToMany(mappedBy = "grades")
        var students: List<Student> = listOf(),
        @OneToMany(mappedBy="grade")
        val childs: Collection<Child> = listOf()
)