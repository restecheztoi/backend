package fr.auchan.restecheztoi.backend.entity

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import fr.auchan.restecheztoi.backend.enumeration.State
import java.time.LocalTime
import java.util.*
import javax.persistence.*

@Entity(name = "course")
class Course(
        @Id
        var id: String = UUID.randomUUID().toString(),
        val date: Date,
        val timeStart: LocalTime,
        val timeEnd: LocalTime,
        @ManyToOne(fetch = FetchType.LAZY)
        val child: Child,
        @ManyToOne(fetch = FetchType.LAZY)
        val student: Student,
        @ManyToOne(fetch = FetchType.LAZY)
        val subject: Subject,
        @Enumerated(EnumType.STRING)
        val state: State
)