package fr.auchan.restecheztoi.backend.entity

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import java.util.*
import javax.persistence.*

@Entity(name = "student")
@JsonIgnoreProperties("subjects", "grades", "courses")
class Student(
        @Id
        var id: String = UUID.randomUUID().toString(),

        @ManyToMany(cascade = [CascadeType.ALL], fetch = FetchType.LAZY)
        @JoinTable(
                name = "student_subject",
                joinColumns = [JoinColumn(name = "studentId", referencedColumnName = "id")],
                inverseJoinColumns = [JoinColumn(name = "subjectId", referencedColumnName = "id")])
        val subjects: List<Subject> = listOf(),

        @ManyToMany(cascade = [CascadeType.ALL], fetch = FetchType.LAZY)
        @JoinTable(
                name = "student_grade",
                joinColumns = [JoinColumn(name = "studentId", referencedColumnName = "id")],
                inverseJoinColumns = [JoinColumn(name = "gradeId", referencedColumnName = "id")])
        val grades: List<Grade> = listOf(),

        @OneToMany(mappedBy="student")
        val courses: Collection<Course> = listOf(),

        val first_name: String,
        val last_name: String,
        val formation: String,
        val school: String,
        val phoneNumber: String
)