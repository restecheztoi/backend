package fr.auchan.restecheztoi.backend.repository

import fr.auchan.restecheztoi.backend.entity.Course
import fr.auchan.restecheztoi.backend.entity.Student
import fr.auchan.restecheztoi.backend.enumeration.State
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository

@Repository
interface CourseRepository : JpaRepository<Course, String> {

    fun findByStudentAndStateOrderByDateDescTimeStartDesc(student: Student, state: State): List<Course>

    @Query("select c1 from course c1 inner join c1.child c2 where c2.parentId = ?1 and c1.state = ?2 order by c1.date desc, c1.timeStart desc")
    fun findByParentIdAndStateOrderByDateDescTimeStartDesc(parentId: String, state: State): List<Course>

    @Query("select c1 from course c1 inner join c1.child c2 where c1.student = ?1 and c2.parentId = ?2 and c1.state = ?3 order by c1.date desc, c1.timeStart desc")
    fun findByStudentAndParentIdAndStateOrderByDateDescTimeStartDesc(student: Student, parentId: String, state: State): List<Course>

}