package fr.auchan.restecheztoi.backend.repository

import fr.auchan.restecheztoi.backend.entity.Student
import fr.auchan.restecheztoi.backend.entity.Subject
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository

@Repository
interface SubjectRepository : CrudRepository<Subject, String> {

    fun findByStudents(student: Student): List<Subject>

}