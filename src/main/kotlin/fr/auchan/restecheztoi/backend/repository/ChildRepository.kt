package fr.auchan.restecheztoi.backend.repository

import fr.auchan.restecheztoi.backend.entity.Child
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository

@Repository
interface ChildRepository : CrudRepository<Child, String> {

    fun findByParentId(parentId: String) : List<Child>

}