package fr.auchan.restecheztoi.backend.service

import fr.auchan.restecheztoi.backend.entity.Course
import fr.auchan.restecheztoi.backend.enumeration.State
import fr.auchan.restecheztoi.backend.repository.CourseRepository
import fr.auchan.restecheztoi.backend.repository.StudentRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Service
import java.util.*

@Service
class CourseService(@Autowired private val courseRepository: CourseRepository, @Autowired private val studentRepository: StudentRepository) {

    fun findAll(studentId: String?, parentId: String?, state: State): List<Course> {
        return studentId?.let { sId ->
            var student = studentRepository.findByIdOrNull(sId) ?: return listOf()
            parentId?.let { pId ->
                courseRepository.findByStudentAndParentIdAndStateOrderByDateDescTimeStartDesc(student, pId, state)
            } ?: courseRepository.findByStudentAndStateOrderByDateDescTimeStartDesc(student, state)
        } ?: parentId?.let {
            courseRepository.findByParentIdAndStateOrderByDateDescTimeStartDesc(it, state)
        } ?: throw IllegalStateException()
    }

    fun create(course: Course): Course {
        course.id = UUID.randomUUID().toString()
        return courseRepository.save(course)
    }

    fun update(id: String, course: Course): Course {
        course.id = id
        return courseRepository.save(course)
    }

}