package fr.auchan.restecheztoi.backend.service

import fr.auchan.restecheztoi.backend.entity.Student
import fr.auchan.restecheztoi.backend.repository.GradeRepository
import fr.auchan.restecheztoi.backend.repository.StudentRepository
import fr.auchan.restecheztoi.backend.repository.SubjectRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.util.*

@Service
class StudentService(@Autowired private val studentRepository: StudentRepository, @Autowired private val gradeRepository: GradeRepository, @Autowired private val subjectRepository: SubjectRepository) {

    fun findById(id: String) = studentRepository.findById(id)

    fun findByGradesAndSubjects(gradeId: String, subjectId: String): List<Student> {
        var grade = gradeRepository.findById(gradeId)
        var subject = subjectRepository.findById(subjectId)

        return if(grade.isPresent && subject.isPresent) {
            studentRepository.findByGradesAndSubjects(grade.get(), subject.get())
        } else {
            listOf()
        }
    }

    fun createOrUpdate(id: String, student: Student): Student {
        student.id = id
        return studentRepository.save(student)
    }

    fun delete(id: String) = studentRepository.delete(studentRepository.findById(id).get())

}