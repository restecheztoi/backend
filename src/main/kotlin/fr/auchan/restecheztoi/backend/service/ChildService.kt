package fr.auchan.restecheztoi.backend.service

import fr.auchan.restecheztoi.backend.entity.Child
import fr.auchan.restecheztoi.backend.repository.ChildRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.util.*

@Service
class ChildService(@Autowired private val childRepository: ChildRepository) {

    fun create(child: Child): Child {
        child.id = UUID.randomUUID().toString()
        return childRepository.save(child)
    }

    fun findByParentId(parentId: String) = childRepository.findByParentId(parentId)

    fun delete(id: String) = childRepository.delete(childRepository.findById(id).get())

}