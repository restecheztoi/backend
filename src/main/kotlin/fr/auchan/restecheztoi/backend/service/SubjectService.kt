package fr.auchan.restecheztoi.backend.service

import fr.auchan.restecheztoi.backend.entity.Grade
import fr.auchan.restecheztoi.backend.entity.Student
import fr.auchan.restecheztoi.backend.entity.Subject
import fr.auchan.restecheztoi.backend.repository.StudentRepository
import fr.auchan.restecheztoi.backend.repository.SubjectRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Service
import java.util.*

@Service
class SubjectService (@Autowired private val subjectRepository: SubjectRepository, @Autowired private val studentRepository: StudentRepository) {

    fun create(subject: Subject): Subject {
        subject.id = UUID.randomUUID().toString()
        return subjectRepository.save(subject)
    }

    fun findById(id: String) = subjectRepository.findById(id)

    fun findByStudents(studentId: String?): List<Subject> {
        return studentId?.let { sId ->
            studentRepository.findByIdOrNull(sId)?.let {
                subjectRepository.findByStudents(it)
            } ?: listOf()
        } ?: subjectRepository.findAll().toList()
    }

    fun update(id: String, subject: Subject): Subject {
        subject.id = id
        return subjectRepository.save(subject)
    }

    fun delete(id: String) = subjectRepository.delete(subjectRepository.findById(id).get())

}