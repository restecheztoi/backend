package fr.auchan.restecheztoi.backend.service

import fr.auchan.restecheztoi.backend.entity.Grade
import fr.auchan.restecheztoi.backend.entity.Student
import fr.auchan.restecheztoi.backend.repository.GradeRepository
import fr.auchan.restecheztoi.backend.repository.StudentRepository
import fr.auchan.restecheztoi.backend.repository.SubjectRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Service
import java.util.*

@Service
class GradeService (@Autowired private val gradeRepository: GradeRepository, @Autowired private val studentRepository: StudentRepository, @Autowired private val subjectRepository: SubjectRepository) {

    fun create(grade: Grade): Grade {
        grade.id = UUID.randomUUID().toString()
        return gradeRepository.save(grade)
    }

    fun findById(id: String) = gradeRepository.findById(id)

    fun findByStudents(studentId: String?): List<Grade> {
        return studentId?.let { sId ->
            studentRepository.findByIdOrNull(sId)?.let {
                gradeRepository.findByStudents(it)
            } ?: listOf()
        } ?: gradeRepository.findAll().toList()
    }

    fun update(id: String, grade: Grade): Grade {
        grade.id = id
        return gradeRepository.save(grade)
    }

    fun delete(id: String) = gradeRepository.delete(gradeRepository.findById(id).get())

}