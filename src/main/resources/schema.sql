insert into student (id, first_name, last_name, formation, phone_number, school) values ('de0247b9-7775-468e-a7ce-07c3d916a8e8', 'Alban', 'Guillet', 'Master mathématiques', '06 62 19 52 48', 'Faculté des sciences, Nantes');
insert into student (id, first_name, last_name, formation, phone_number, school) values ('6d239c13-1fad-445c-87e0-9ce05439bdd0', 'Amélie', 'Luneau', 'L2 informatique', '06 62 19 52 47', 'Faculté des sciences, Nantes');
insert into student (id, first_name, last_name, formation, phone_number, school) values ('e2616fe5-0709-42db-a247-be0925ae41b2', 'Yann', 'Durand', 'M1 histoire', '06 62 19 52 46', 'Faculté Histoire, Nantes');

insert into grade (id, label) values ('6D8844A3-A113-4C03-ABA4-0E10ABAEA4E3', 'CP');
insert into grade (id, label) values ('118F5952-3FE0-48FA-891E-421CAD9B452D', 'CE1');
insert into grade (id, label) values ('F8B3333E-2C8C-455D-9B82-235B9F45D383', 'CE2');
insert into grade (id, label) values ('0C5FA3B1-D813-4067-B5FB-CD388080623D', 'CM1');
insert into grade (id, label) values ('C7FC6622-1B7C-412B-929B-D24A9D4162C0', 'CM2');
insert into grade (id, label) values ('EA78DD58-DB70-4A14-9B5A-4BF53E390C7E', '6e');
insert into grade (id, label) values ('6B19E53C-58C7-4C60-8830-1E5AAB7D9738', '5e');
insert into grade (id, label) values ('54F2B312-349D-4838-88C5-6B36E591126D', '4e');
insert into grade (id, label) values ('2A2325EB-3607-4FE2-A17F-2E2E23B4AE66', '3e');

insert into subject (id, label) values ('283F5EAC-2B84-4CF1-8592-5875F1E997EA', 'Mathématiques');
insert into subject (id, label) values ('ADE81F9E-69F5-4796-A948-1FF0A4B7FCB5', 'Français');
insert into subject (id, label) values ('C17F7FCE-5CA8-47D2-BF2B-9475E6B2C09E', 'Histoire');
insert into subject (id, label) values ('92DC80AB-E572-425F-ACB7-2693022C442D', 'Anglais');
insert into subject (id, label) values ('C20B99F2-75E2-43B5-B111-029AB4017283', 'Géographie');

insert into student_grade (grade_id, student_id) values ('6D8844A3-A113-4C03-ABA4-0E10ABAEA4E3', 'de0247b9-7775-468e-a7ce-07c3d916a8e8');
insert into student_grade (grade_id, student_id) values ('6D8844A3-A113-4C03-ABA4-0E10ABAEA4E3', '6d239c13-1fad-445c-87e0-9ce05439bdd0');
insert into student_grade (grade_id, student_id) values ('F8B3333E-2C8C-455D-9B82-235B9F45D383', 'e2616fe5-0709-42db-a247-be0925ae41b2');

insert into student_subject (subject_id, student_id) values ('283F5EAC-2B84-4CF1-8592-5875F1E997EA', 'de0247b9-7775-468e-a7ce-07c3d916a8e8');
insert into student_subject (subject_id, student_id) values ('283F5EAC-2B84-4CF1-8592-5875F1E997EA', '6d239c13-1fad-445c-87e0-9ce05439bdd0');
insert into student_subject (subject_id, student_id) values ('283F5EAC-2B84-4CF1-8592-5875F1E997EA', 'e2616fe5-0709-42db-a247-be0925ae41b2');
insert into student_subject (subject_id, student_id) values ('ADE81F9E-69F5-4796-A948-1FF0A4B7FCB5', 'de0247b9-7775-468e-a7ce-07c3d916a8e8');